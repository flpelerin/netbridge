#define _POSIX_C_SOURCE >= 199309L	/* nanosleep */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>


#define SLEEP_MS	100
#define BUFFER_SIZE	4096


static inline void mssleep(int ms) { nanosleep(&(struct timespec) {ms / 1000, (ms % 1000) * 1000000}, &(struct timespec){0}); }


void error (char *msg, int *fd, int len) {
	perror (msg);
	for (int i = 0; i < len; ++i)
		close (fd[i]);
	exit (EXIT_FAILURE);
}

#define NUMARGS(...)  (sizeof((int[]){__VA_ARGS__}) / sizeof(int))
#define error(M, ...) error(M, (int[]){__VA_ARGS__}, NUMARGS(__VA_ARGS__))


int host (char *addr, int port) {
	int connfd, sockfd;
	struct sockaddr_in server_addr, client_addr;


	connfd = socket (AF_INET, SOCK_STREAM, 0);
	if (connfd == -1) error ("Socket creation failed");
	setsockopt (connfd, SOL_SOCKET, SO_REUSEADDR, &(int){1}, sizeof (int));


	memset (&server_addr, 0, sizeof (struct sockaddr_in));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons (port);
	server_addr.sin_addr.s_addr = INADDR_ANY;


	if (bind (connfd, (struct sockaddr *) &server_addr, sizeof (struct sockaddr)) == -1)  error ("Bind failed", connfd);


	fprintf (stderr, "Server listening on port %d for connection from %s\n", port, addr);


listen:	if (listen (connfd, 1) == -1) error ("Listen failed", connfd);


	sockfd = accept (connfd, (struct sockaddr *) &client_addr, &(unsigned) {(unsigned) sizeof (struct sockaddr)});
	if (sockfd == -1) error ("Failed to accept connection from client", connfd, sockfd);

	char client_ip[INET_ADDRSTRLEN];
	inet_ntop (AF_INET, &client_addr.sin_addr, client_ip, sizeof (client_ip));


	if (strcmp (client_ip, addr) != 0) {
		fprintf(stderr, "Connection rejected from unauthorized IP: %s\n", client_ip);
		close (sockfd);
		goto listen;
	}


	fprintf (stderr, "Received client %s, port %d, socket %d\n", client_ip, port, sockfd);


	return sockfd;
}


int join (char *addr, int port) {
	int sockfd;
	struct sockaddr_in server_addr;


	fprintf (stderr, "Trying to connect to server at %s, port %d\n", addr, port);


	sockfd = socket (AF_INET, SOCK_STREAM, 0);
	if (sockfd == -1) error ("Socket creation failed");


	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons (port);
	inet_pton (AF_INET, addr, &server_addr.sin_addr);

	if (connect (sockfd, (struct sockaddr *) &server_addr, sizeof (struct sockaddr)) == -1) return -1;


	fprintf (stderr, "Connected to server %s, port %d, socket %d\n", addr, port, sockfd);


	return sockfd;
}


int main (int argc, char *argv[]) {
	if (argc < 4) {
		fprintf (stderr, "Syntax: %s: [IP] [port] [file]\n", argv[0]);
		exit (EXIT_FAILURE);
	}


	char *addr = argv[1],
	     *path = argv[3];
	int   port = atoi (argv[2]);

	int sockfd = join (addr, port);
	if (sockfd == -1) {
		fprintf (stderr, "\nFailed to connect to server, now entering host mode...\n");
		close (sockfd);
		sockfd = host (addr, port);
	}

	int flags = fcntl(sockfd, F_GETFL, 0);
	fcntl(sockfd, F_SETFL, flags | O_NONBLOCK);


	int bridgefd = open (argv[3], O_CREAT | O_RDWR | O_TRUNC, 0644);
	fprintf (stderr, "\nOpening bridge file %s, descriptor %d\n", path, bridgefd);
	if (bridgefd == -1) error ("Failed opening bridge file");


	fprintf (stderr, "\n");
	char data[BUFFER_SIZE] = "";
	for (; ; mssleep (SLEEP_MS)) {
		char buffer[sizeof (data)] = "";
		lseek (bridgefd, 0, SEEK_SET);
		int nbytes;

		if ((nbytes = recv (sockfd, buffer, sizeof (buffer), 0)) == 0) goto exit;
		if (nbytes > 0) {
			printf ("from %s: %s\n", addr, buffer);

			strcpy (data, buffer);
			write (bridgefd, buffer, sizeof (buffer));
			
			continue;
		}

		read (bridgefd, buffer, sizeof (buffer));
		if (strcmp (buffer, data) != 0) {
			printf ("to %s: %s\n", addr, buffer);

			strcpy (data, buffer);
			write (sockfd, buffer, sizeof (buffer));
			
			continue;
		}
	}

exit:		fprintf (stderr, "Connection terminated\n");
		close (sockfd);
		close (bridgefd);
		exit (EXIT_SUCCESS);
}
