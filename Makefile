CC	:= gcc
CFLAGS	:= -static -O3 -Wall -Wextra -std=c99
CLIBS	:=

TARGET	:= main
SRC	:= *.c



all:		$(TARGET)

clean:	
		$(RM) $(TARGET)

run:		$(TARGET)
	./$<

install:	$(TARGET)
	mv $< /bin/netbridge
	ln -s /bin/netbridge /bin/nb



$(TARGET):	$(SRC)
	$(CC) $(CFLAGS) -o $@ $^ $(CLIBS)
