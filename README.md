

## NetworkBridge (NB)

    All in one TCP server and client, redirecting over-the-network packages to files.
    Permits syncing two files accross a network.



### Syntax: nb [IP] [port] [file]

    - Both client and server mode uses the same syntax.
    - Resembles a peer-to-peer model, as the host / client attribution is implicit.



### Examples:

    nb 172.0.0.1 4444 /tmp/nb0          -> host
    nb 172.0.0.1 4444 /tmp/nb1          -> client

    echo "Hi!" > /dev/nb0
    cat /dev/nb1                        -> "Hi!"



### Notes:
    Once one instance of nb is terminated, the other session will close automatically.
    The "server" will only accept IP addresses given as argument. It will refuse all other addresses.
